#include <stdio.h>
#include <math.h>
#include <stdlib.h>


//STRUCTURE DE DONNEES----------------------------------------------------------
typedef struct{
  int arrivee;
  int temps_attente;
  int depart;
} client;//Structure d'un client, identifié par sa date d'arrivée, son temps d'attente et son heure de depart
typedef struct liste_chaine{
  client cust;
  struct liste_chaine *suiv;
} chaine;//Structure de liste chainée pour avoir une liste infinie de clients
typedef struct{
  chaine *debut;
  chaine *fin;
} file;//Structure de file : un pointeur de début et un pointeur de fin de liste
//------------------------------------------------------------------------------
//STRUCTURE DES FONCTIONS-------------------------------------------------------
//Fonctions coeur---------------
void enfile(file *f,chaine *i);//Enfile la chaine i dans la file f
void defile(file *f);//Defile la file f
int clientSuivant(float lambda);//Génère le temps d'arrivée du client suivant selon la loi exponentielle
void creerListe(file *f,float lambda, int min, int max);//Crée la file
void ecrireListe(file *f, char *filename);//Rédige la file dans un fichier, un client par ligne
void lireListe(file *f,char * filename);//Lis un fichier et en crée une file
//statistiques------------------
float taille_File(file *f);
float taillemoyenne(file *f);
int taillemax(file *f);
float debit(file *f);
float tauxNonServies(file *f);
float traitement_moyen(file *f);
//affichage---------------------
void afficheFile(file *f);//Affiche une file
void Menu();
void sousMenu(file *f);
//------------------------------------------------------------------------------
//MAIN--------------------------------------------------------------------------
void main(){
  Menu();
}
//------------------------------------------------------------------------------
//FONCTIONS---------------------------------------------------------------------
//Fonctions coeur---------------
//Enfile la chaine i dans la file f
void enfile(file *f,chaine *i){
  if(f->debut==NULL){
    f->debut=i;
    f->fin=i;
    return;
  }
  f->fin->suiv=i;
  f->fin=i;
}
//Defile la file f
void defile(file *f){
  f->debut=f->debut->suiv;
}
//Génère le temps d'arrivée du client suivant selon la loi exponentielle
int clientSuivant(float lambda){
  float rdm01=(float)rand()/(float)(RAND_MAX);
  return((int)((-log(1.0-rdm01)/lambda)));
}
//Crée la file
void creerListe(file *f,float lambda, int min, int max){
  //Initialisation debut et fin de file comme l'attente d'un client dépend du client précédent
  chaine *Client1;
  Client1=(chaine*)malloc(sizeof(chaine));
  Client1->cust.arrivee=clientSuivant(lambda)+510;
  int heure=Client1->cust.arrivee;
  int traitement1 = min+(int)(max-min)*(float)rand()/(float)(RAND_MAX);
  Client1->cust.depart=Client1->cust.arrivee+traitement1;
  Client1->cust.temps_attente=0;
  Client1->suiv=NULL;
  f->debut=Client1;
  f->fin=Client1;
  //On ajoute les clients tant que l'heure d'arrivée du prochain client soit inférieure à 1020=17h
  while(heure<1020){
    int suivant=clientSuivant(lambda);
    if(suivant+heure>1020)
      break;
    client nvClient;
    nvClient.arrivee=heure+suivant;
    int traitement = min+(int)((max-min)*(float)rand()/(float)(RAND_MAX));
    int attente=f->fin->cust.depart-nvClient.arrivee;
    if(attente<0)
      attente=0;
    int depart=nvClient.arrivee+attente+traitement;
    nvClient.depart=depart;
    nvClient.temps_attente=attente;
    chaine *newcust;
    newcust=(chaine*)malloc(sizeof(chaine));
    newcust->cust=nvClient;
    newcust->suiv=NULL;
    enfile(f,newcust);
    heure+=suivant;
  }
}
//Rédige la file dans un fichier, un client par ligne
void ecrireListe(file *f, char *filename){
  FILE *fichier;
  fichier=fopen(filename,"w");
  chaine *cust;
  cust=f->debut;
  //parcours de la file
  while(cust!=f->fin->suiv){
    int arr=cust->cust.arrivee;
    int dep=cust->cust.depart;
    int attente=cust->cust.temps_attente;
    fprintf(fichier, "%d %d %d\n",arr,attente,dep);
    cust=cust->suiv;
  }
  fclose(fichier);
}
//Lis un fichier et en crée une file
void lireListe(file *f,char * filename){
  FILE *fichier;
  fichier=fopen(filename,"r");
  if(fichier==NULL){printf("\n-Mauvais Nom-\n");return;}
  int arrivee;
  int attente;
  int depart;
  while(fscanf(fichier,"%d%d%d",&arrivee,&attente,&depart)==3){
    client cust;
    cust.arrivee=arrivee;
    cust.temps_attente=attente;
    cust.depart=depart;
    chaine *newcust;
    newcust=(chaine *)malloc(sizeof(chaine));
    newcust->cust=cust;
    newcust->suiv=NULL;
    enfile(f,newcust);
  }
}
//statistiques------------------
//Renvoie la taille d'une file
float taille_File(file *f){
  chaine * c=f->debut;
  float t=0;
  while(c!=NULL){
    t+=1;
    c=c->suiv;
  }
  return(t);
}
//Renvoie la taille moyenne des files sur une journée
float taillemoyenne(file *f){
  chaine *cust;//pointeur du prochain client (pas encore dans la file d'attente)
  cust=f->debut;
  float taille=0;
  float tailletotale=0;
  chaine *premierClient=f->debut;//pointeur de début de file d'attente
  //On parcourt de 8h30 à 17h30
  for(int k=510;k<=1050;k++){
    //si le premier client dans la file d'attente est le dernier client de la journée
    if(premierClient->suiv==NULL){
      if(k==premierClient->cust.arrivee+premierClient->cust.temps_attente)//on attend qu'il se fasse traiter et la journée se finit
        return(tailletotale/510.0);
      tailletotale=tailletotale+taille;
      continue;
    }
    //S'il n'y a pas de prochain client ie tous les clients sont arrivés
    if(cust==NULL||cust->suiv==NULL){
      if(k==premierClient->cust.arrivee+premierClient->cust.temps_attente){
        premierClient=premierClient->suiv;
        taille-=1;
        if(premierClient==NULL)
        return(tailletotale);
      }
      tailletotale=tailletotale+taille;
      continue;
    }
    //vérifie si un ou plusieurs clients arrivent
    while(k==cust->cust.arrivee){
      taille+=1;
      cust=cust->suiv;
      if(cust==NULL)//Au cas où les 2 derniers clients arrivent en même temps
        break;
    }
    //vérifie si le client peut être servi
    if(k==premierClient->cust.arrivee+premierClient->cust.temps_attente){
      premierClient=premierClient->suiv;
      taille-=1;
      if(premierClient==NULL)
      return(tailletotale);
    }
    tailletotale=tailletotale+taille;
  }
  tailletotale=tailletotale/540.0;
  return(tailletotale);
}
//Renvoie la taille maximale des files sur une journée
int taillemax(file *f){
  //Voir taillemoyenne pour les commentaires (code similaire)
  chaine *cust;
  cust=f->debut;
  int taille=0;
  int MAXt=0;
  chaine *premierClient=f->debut;
  for(int k=510;k<=1050;k++){
    if(premierClient->suiv==NULL){
      if(k==premierClient->cust.arrivee+premierClient->cust.temps_attente)
        return(MAXt);
      if(taille>MAXt){
        MAXt=taille;
      }
      continue;
    }
    if(cust==NULL||cust->suiv==NULL){
      if(k==premierClient->cust.arrivee+premierClient->cust.temps_attente){
        premierClient=premierClient->suiv;
        taille-=1;
        if(premierClient==NULL)
        return(MAXt);
      }
      continue;
    }
    while(k==cust->cust.arrivee){
      taille+=1;
      cust=cust->suiv;
      if(cust==NULL)
        break;
    }
    if(k==premierClient->cust.arrivee+premierClient->cust.temps_attente){
      premierClient=premierClient->suiv;
      taille-=1;
      if(premierClient==NULL)
      return(MAXt);
    }
    if(taille>MAXt){
      MAXt=taille;
    }
  }
  return(MAXt);
}
//Renvoie le débit moyen des clients sur une journée
float debit(file *f){
  return((float)taille_File(f)/510.0);
}
//Renvoie le pourcentage de clients non servies
float tauxNonServies(file *f){
  chaine *c=f->debut;
  float t=0;
  float taille;
  while(c!=NULL){
    taille++;
    if(c->cust.depart>1050)//le client a-t-il un départ prévu après la fermeture
      t+=1;
    c=c->suiv;
  }
  return(t/taille);
}
//Renvoie le temps de traitement moyen
float traitement_moyen(file *f){
  chaine *c=f->debut;
  float t=0;
  float taille;
  while(c!=NULL){
    taille++;
    t+=c->cust.depart-(c->cust.arrivee+c->cust.temps_attente);//temps de traitement
    c=c->suiv;
  }
  return(t/taille);
}
//Affichage---------------------
//Affiche une file
void afficheFile(file *f){
  chaine *c;
  c=f->debut;
  int t=1;
  while(c!=NULL){
    int arr=c->cust.arrivee;
    int attente =c->cust.temps_attente;
    int dep=c->cust.depart;
    printf("\n-Client #%d- -Arrivee: %d- -Temps d'attente: %d- -Depart: %d-",t,arr,attente,dep);
    t++;
    c=c->suiv;
  }
  printf("\n");
}
//Sous Menu
void sousMenu(file *f){
  printf("\n1)Afficher l'ensemble des statistiques de ce guichet\n2)Enregistrer les statistiques dans un fichier\n3)Afficher la liste\n4)Enregistrer la liste dans un fichier\n5)Revenir au Menu\n>");
  int i;
  scanf("%d",&i);
  char filename[100];
  switch (i) {
    default:
    free(f);
    Menu();
    return;
    case 1:
    printf("\nTaille moyenne des files d'attente: %f\n",taillemoyenne(f));
    printf("Taille maximale des files d'attente: %d\n",taillemax(f));
    printf("Debit moyen: %f\n",debit(f));
    printf("Taux de clients non servis: %f\n",tauxNonServies(f));
    printf("Temps de reponse moyen: %f\n",traitement_moyen(f));
    sousMenu(f);
    return;
    case 2:
    printf("\nNom du fichier\n>");
    scanf("%s",filename);
    FILE *tab=fopen(filename,"w");
    fprintf(tab,"Pour la liste %s\n\n",filename);
    fprintf(tab,"Taille moyenne des files d'attente: %f\n",taillemoyenne(f));
    fprintf(tab,"Taille maximale des files d'attente: %d\n",taillemax(f));
    fprintf(tab,"Debit moyen: %f\n",debit(f));
    fprintf(tab,"Taux de clients non servis: %f\n",tauxNonServies(f));
    fprintf(tab,"Temps de reponse moyen: %f\n",traitement_moyen(f));
    fclose(tab);
    printf("\n-fichier %s cree-\n",filename);
    sousMenu(f);
    return;
    case 3:
    afficheFile(f);
    sousMenu(f);
    return;
    case 4:
    printf("\nNom du fichier\n>");
    scanf("%s",filename);
    ecrireListe(f,filename);
    printf("\n-fichier %s cree-\n",filename);
    sousMenu(f);
    return;
  }
}
//Menu principal
void Menu(){
  file *f;
  f=(file*)malloc(sizeof(file));
  f->debut=NULL;
  f->fin=NULL;
  int want=1;//input
  printf("\nVoulez-vous\n  1)Generer une file d'attente ?\n  2)Ouvrir un fichier de liste d'attente ?\n  3)Quitter\n(un client s'identifie par son heure d'arrivee, son temps d'attente et son heure de depart)\n>");
  scanf("%d",&want);
  char filename[100];
  switch (want) {
    default://Quitter
    free(f);
    return;
    case 1://acquisition et génération
    printf("\nParametres de generation:\n -temps d'arrivee d'un client apres un autre:\n    Loi exponentielle de parametre lambda: ");
    float lambda=0.08;
    int duree_min=5;
    int duree_max=20;
    scanf("%f",&lambda);
    printf(" -temps minimal de traitement d'un client (en minutes): ");
    scanf("%d",&duree_min);
    printf(" -temps maximal de traitement d'un client (en minutes): ");
    scanf("%d",&duree_max);
    creerListe(f,lambda,duree_min,duree_max);
    printf("\n-liste generee-\n");

    sousMenu(f);
    break;
    case 2://lancement de la lecture
    printf("\nNom du fichier\n>");
    scanf("%s",filename);
    lireListe(f,filename);
    if(f->debut==NULL)//reset en cas d'erreur
    {free(f);Menu(f);return;}
    printf("\n-liste lue-\n");
    sousMenu(f);
    break;
  }
}
//------------------------------------------------------------------------------
